# Release Notes
2024-10-17

## Added

* New function for CRM customer ID module.

## Changed

* The Survey Addon function now registers dialplan DTMF answer directly in PostgreSQL
* Call events logs and channel notifications for Wallboard Addon improvements

## Fixed

## Removed